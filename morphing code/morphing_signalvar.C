Double_t normf(Double_t *x, Double_t *par)
{
Float_t xx =x[0];
Double_t val = (139e12)*(par[0] * pow(xx,(par[1]-4)) * pow(1 - (xx/13),par[2])) * (par[3] /(1 + TMath::Exp((par[4]-xx)*par[5])*(1 + TMath::Exp((par[6]-xx)*par[7]))));
return val;
}

using   namespace   RooFit ;
#include "/Applications/root_v6.20.04/macros/atlasstyle-00-04-02/AtlasStyle.C"

void morphing_signalvar(){
 SetAtlasStyle();
 RooWorkspace *w = new RooWorkspace("w");
 w->factory("mu[0,10]");
 RooRealVar *mu = w->var("mu");
 RooArgList pdfs; 
 
 const Int_t NBINS = 63; // Variable Binning
 Double_t edges[NBINS + 1] = {300, 347, 395, 443, 491, 540, 589, 639, 689, 740, 791, 843, 895, 948,1002, 1056, 1111, 1167, 1224, 1281, 1339, 1398, 1458, 1519, 1581, 1644,1708, 1773, 1839, 1906, 1974, 2043, 2114, 2186, 2259, 2333, 2409, 2486,2564, 2644, 2725, 2808, 2892, 2978, 3065, 3154, 3244, 3336, 3430,3526, 3623, 3722, 3823, 3926, 4031, 4138, 4247, 4358, 4472, 4588,4706, 4826, 4949, 5000};
///////////////////////////////////////////////////////////////////////////////

 Float_t m_morph = 2.75; // Interpolated mass to choose (in TeV) between 1.5 and 3.5 TeV
 TString model= "D"; // Model to choose
 
// The file corresponding is save as 'inter_model-_- - - -.root'
 
///////////////////////////////////////////////////////////////////////////////
 TVectorD masses =TVectorD(5);
 masses[0]=1500;
 masses[1]=2000;
 masses[2]=2500;
 masses[3]=3000;
 masses[4]=3500;
 
 Float_t alpha=(m_morph-1.5)*2; 
 
 std::vector <TH1D*> hist;
 std::vector <TH1D*> histb;

 RooRealVar mjj("mjj","mjj_[GeV]",300,5000);
 RooBinning mjjbins(4700, 300,5000, "mjjbins");
 mjj.setBinning(mjjbins);   

 Double_t norm=1.;

for(int j=0; j<5; j++){
  Int_t mass = masses[j];
  TH1D* h = new TH1D("h", "h", 4700, 300,5000);
  TH1D* hb = new TH1D("h", "h", NBINS,edges);
  TFile* f= new TFile(Form("/Users/thomas/Downloads/FICHIER_ROOT/new_file/new_model%s_%d.root",model.Data(),mass),"READ");
  Float_t m=0.;
  Float_t w=0.;
 
  TTree *Tree = (TTree*)f->Get("nominal");
  Tree->SetBranchAddress("mjj", &m);
  Tree->SetBranchAddress("TotalEventWeight", &w);

  int nentries = Tree->GetEntries();
  for(int j=0; j<nentries; j++){
    Tree->GetEntry(j);
          h->Fill(m, w);  
          hb->Fill(m,w);
  }
  hist.push_back(h);
  histb.push_back(hb);
}

 RooDataHist datahist1("datahist1","datahist1",mjj,hist[0]);
 RooHistPdf pdf1("pdf1","pdf1",mjj,datahist1);
 pdfs.add(pdf1);

 RooDataHist datahist2("datahist2","datahist2",mjj,hist[1]);
 RooHistPdf pdf2("pdf2","pdf2",mjj,datahist2);
 pdfs.add(pdf2);

 RooDataHist datahist3("datahist3","datahist3",mjj,hist[2]);
 RooHistPdf pdf3("pdf3","pdf3",mjj,datahist3);
 pdfs.add(pdf3);

 RooDataHist datahist4("datahist4","datahist4",mjj,hist[3]);
 RooHistPdf pdf4("pdf4","pdf4",mjj,datahist4);
 pdfs.add(pdf4);

 RooDataHist datahist5("datahist5","datahist5",mjj,hist[4]);
 RooHistPdf pdf5("pdf5","pdf5",mjj,datahist5);
 pdfs.add(pdf5);

 TVectorD paramVec =TVectorD(5);
 paramVec[0]=0;
 paramVec[1]=1;
 paramVec[2]=2;
 paramVec[3]=3;
 paramVec[4]=4;

 mu = w->var("mu");
 RooArgList varlist;
 varlist.add(mjj);
 
 RooMomentMorph *morph = new RooMomentMorph("morph","morph",*mu,varlist,pdfs,paramVec,RooMomentMorph::Linear);
 
 w->import(*morph);
 mu->setVal(alpha);
 
 TH1* hmorph= morph->createHistogram("morph",mjj);
 TH1 *hnew = hmorph->Rebin(NBINS, "hnew", edges);
 
 Double_t scale = norm/(hnew->Integral());
 hnew->Scale(scale);

  Double_t pA[8] = {6.19263369e-15,  -2.96384318e-01,   1.10641054e+01,  1.38041873e+03,   1.61943317e+02,   4.66397446e-02,   1.45465115e+00,   3.07265250e+00};
  Double_t pB[8] = {6.21834369e-15,  -2.99675910e-01,   1.09773378e+01,  0.86421091,       1.46145333,       1.89397673,       1.44278641,       33.25822177 };
  Double_t pC[8] = {6.26139266e-15,  -2.88339265e-01,   1.10900425e+01,  0.8557196 ,       1.62065629,       1.74812668,       1.48726834,       5.22026564};
  Double_t pD[8] = {6.52787657e-15,  -1.75189618e-01,   1.17496008e+01,  6.36532846e+02,   1.69910117e+02,   3.98837466e-02,   1.63131746e+00,   2.37832477e+00};

  TF1 *f = new TF1("f", normf, 1.5, 3.5, 8);
  if(model=="A"){f->SetParameters(pA);}
  if(model=="B"){f->SetParameters(pB);}
  if(model=="C"){f->SetParameters(pC);}
  if(model=="D"){f->SetParameters(pD);}

   hnew->Scale(f->Eval(m_morph));

 TFile *fmorph = new TFile(Form("inter_model%s_%.12g.root",model.Data(),m_morph*1000),"RECREATE");
 TTree *tree = new TTree("nominal", "nominal");
 
 Float_t dijetmass;
 tree->Branch("mjj",&dijetmass);
 
 Float_t weight;
 tree->Branch("TotalEventWeight",&weight);
 
 for(int l=1; l<=hnew->GetNbinsX(); l++){
    dijetmass=hnew->GetBinCenter(l);
    weight=hnew->GetBinContent(l);
    tree->Fill();
  }
  fmorph->Write();

 
auto c2 = new TCanvas("c2","c2");
gPad->SetLogy();
histb[0]->Draw("HIST");
histb[1]->Draw("HIST SAME");
histb[2]->Draw("HIST SAME");
histb[3]->Draw("HIST SAME");
histb[4]->Draw("HIST SAME");
hnew->Draw("HIST SAME");

histb[0]->GetYaxis()->SetTitle("Arbitrary Units");
histb[0]->GetXaxis()->SetTitle("m_{JJ} [GeV]");
  
auto legend_2 = new TLegend(0.7,0.55,0.9,0.85);
legend_2->SetTextSize(0.035);
legend_2->SetFillStyle(0);
legend_2->SetFillColor(0);
legend_2->SetBorderSize(0);
legend_2->SetTextFont(42);

legend_2->AddEntry(histb[0],Form("%s, 1.5 TeV",model.Data()),"l"); 
legend_2->AddEntry(histb[1],Form("%s, 2 TeV",model.Data()),"l"); 
legend_2->AddEntry(histb[2],Form("%s, 2.5 TeV",model.Data()),"l"); 
legend_2->AddEntry(histb[3],Form("%s, 3 TeV",model.Data()),"l"); 
legend_2->AddEntry(histb[4],Form("%s, 3.5 TeV",model.Data()),"l"); 
legend_2->AddEntry(hnew,Form("%.12g TeV Pred.",m_morph),"l"); 

  
histb[0]->SetLineColor(kRed);
histb[0]->SetMarkerColor(kRed);

histb[1]->SetLineColor(kGreen);
histb[1]->SetMarkerColor(kGreen);

histb[2]->SetLineColor(kBlue);
histb[2]->SetMarkerColor(kBlue);

histb[3]->SetLineColor(kViolet);
histb[3]->SetMarkerColor(kViolet);

histb[4]->SetLineColor(kBlack);
histb[4]->SetMarkerColor(kBlack);

hnew->SetLineColor(12);
hnew->SetMarkerColor(12);

hnew->SetLineStyle(7);
hnew->SetLineWidth(3);

legend_2->Draw();
}