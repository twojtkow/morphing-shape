# Signal Interpolation

In the directory 'morphing code', there is the macro 'morphing_signalcst.C', which allows to predict intermediated signals with a constant bins width (100 GeV/bin). The document 'morphing_shape.pdf' describes the principle of the code.

To use the macro, open ROOT in the terminal, and execute the macro by typing :

```javascript
root [0] .x morphing_signalcst.C
```
To choose the interpolated mass, change the real number 'm_morph' between 1.5 and 3.5 (TeV) to get the wanted mass. The Model can be change by modifying the string 'model'.

There is also the macro 'morphing_signalvar.C', which now permits to create the predicted histogram with a variable bins width. 
The steps are similar, but with a difference : the histograms and the predicted one are create with a constant bin width (1 GeV/bin.). The bins of the predicted histogram are then merged in order to get the wanted variable bins width. 

The interpolated signal is saved in a .root file as 'inter_modelA_1750.root' for instance.